package main

import (
	"fmt"
	"math"
)

func main() {
	var Prin, numOfYears, rate float64
	// Prin - Principal
	var periodicity float64 = 12
	// how many times in year
	var totalPayment float64 = 0.0
	var brac float64

	fmt.Print("Enter amount of loan : ")
	fmt.Scan(&Prin)

	fmt.Print("Enter period in years : ")
	fmt.Scan(&numOfYears)

	fmt.Print("Enter the rate : ")
	fmt.Scan(&rate)
	rate = rate * 0.01

	brac = (1 - (math.Pow(1+rate/periodicity, -periodicity*numOfYears))) / (rate / periodicity)
	// it placed before getting inputs, so in zoom brac didn't work
	totalPayment = Prin / brac

	fmt.Printf("\nM : PaymentA : InterestA : PrincipalA : Balance Owed\n")
	payment(Prin, periodicity, numOfYears, rate, totalPayment)

}

func payment(Prin, periodicity, numOfYears, rate, totalPayment float64) {
	var IA, PA, Bal float64 = 0.0, 0.0, 0.0
	// var s []float64
	//IA-interest amount,
	//PA-principal amount,
	//Bal-balance

	Bal = Prin
	month := 1
	for month <= int(numOfYears)*int(periodicity) {
		IA = Bal * rate / periodicity
		PA = totalPayment - IA
		Bal = Bal - PA

		// s = append(s, IA, PA, Bal)
		// method with appending elems to slice
		// I didn't used this method, because it was difficult to print elems in rows

		fmt.Printf("%v: %f, %f, %f, %f\n", month, totalPayment, PA, IA, Bal)
		month++
	}

}
