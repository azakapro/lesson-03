package main

import (
	"fmt"
	"math"
)

func main() {
	fmt.Println(Sqrt(15))
}

func Sqrt(n float64) float64 {
	var left float64 = 1
	right := n
	middle := (left + right) / 2

	for math.Abs(middle*middle-n) > 0.00000001 {
		if middle*middle-n < 0 {
			left = middle
			middle = (left + right) / 2
		} else {
			right = middle
			middle = (left + right) / 2
		}
	}
	return middle
}
